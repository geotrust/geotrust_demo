#!/usr/bin/env python
import os
from setuptools import setup


def localopen(fname):
    return open(os.path.join(os.path.dirname(__file__), fname))


setup(
    name='geotrust_demo',
    version='0.1',
    description='A demo website to execute a sciunit',
    author='Zhihao Yuan',
    author_email='lichray@gmail.com',
    packages=['geotrust_demo'],
    include_package_data=True,
    zip_safe=False,
    license='ASL 2.0',
    keywords=['geotrust', 'flask'],
    url='https://bitbucket.org/geotrust/geotrust_demo',
    long_description=localopen('README.md').read(),
    install_requires=localopen('requirements.txt').readlines(),
    tests_require=localopen('test-requirements.txt').readlines(),
    classifiers=[
        'Development Status :: 1 - Planning',
        'License :: CC0 1.0 Universal (CC0 1.0) Public Domain Dedication',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: Implementation :: PyPy',
    ]
)
