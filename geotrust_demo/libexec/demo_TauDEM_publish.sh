#!/bin/sh

echo "Running sciunit-CLI..."
#cd /home/tonhai/Data/Working/Workspace/Temp/Depaul/food-inspections-evaluation-master_experiment
sudo sciunit-cli.py << EOF
--sciunit start Mysciunit
--publish 84e7684bd4aadc56f68128850022e702f7010850 
Geounit replication of workflow for preparation of Utah Energy Balance (UEB) model input for the Logan River using HydroDS data services.
Utah Energy Balance Model Geounit
UEB, Hydrogate, HydroDS, terrain
y
--stop
EOF
echo "Finished!"

