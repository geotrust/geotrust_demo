function create_remote_job(job_type) {
    var frm = window.frames['content']
    var current = frm.location
    var st = new Xhr(current + "/" + job_type, '', 'POST', 5)
    st.onready = function () {
        var doc = frm.document
        var out = doc.getElementById('output')
        var monitor_id = null
        var endpoint = st.getHeader('Location')

        $del(prior_to(out))
        set_text(out, '')
        function update_monitor() {
            var st = new Xhr(endpoint, '', 'GET', 2)
            st.onready = function() {
                append_text(out, st.getText())
                frm.scrollTo(0, frm.scrollMaxY)
            }
            st.onfail = function() {
                clearInterval(monitor_id)
            }
            st.start()
        }
        monitor_id = setInterval(update_monitor, 200)
    }
    st.start()
}

function prior_to(node) {
    var ls = []
    for (var o = node.previousElementSibling;
         o != null;
         o = o.previousElementSibling)
        ls.push(o)
    return ls
}

function bind_all() {
    var buttons = window.frames['related']
        .document.getElementsByClassName("btn-big-red")
    buttons[0].onclick = function () { create_remote_job("package") }
    buttons[1].onclick = function () { create_remote_job("publish") }
    buttons[2].onclick = function () { create_remote_job("repeat") }
}

window.onload = bind_all
