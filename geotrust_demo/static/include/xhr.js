/**
 * Copyright (c) 2010-2017, Zhihao Yuan
 * All rights reserved.
 *
 * xhr.js - XML HTTP Requirer
 *
 * Filename: xhr.js
 * Version:  1.4
 * Author:   Zhihao Yuan
 * Licence:  Simplified BSD License
 *
 */

function Xhr(uri, params, via, timeout) {
	this.setParams = function(p) {
		params = param_format(p)
	}

	this.setParams(params)
	via == 'POST' ? via : via = 'GET'
	this.status = NaN
	this.onready = Function()
	this.onfail = Function()
	var self
	if (timeout) this.ontimeout = Function()

	if (window.XMLHttpRequest)
		self = new XMLHttpRequest()
	else {
		var atps = ['MSXML2.XMLHTTP.5.0', 'MSXML2.XMLHTTP.4.0', 
			'MSXML2.XMLHTTP.3.0', 'MSXML2.XMLHTTP', 'Microsoft.XMLHTTP']
		while (kw = atps.shift()) {
			try {
				self = new ActiveXObject(kw)
				break
			} catch (e) {
				if (atps == false) throw e
			}
		}
	}

	this.start = function() {
		var xhr = this
		var timer
		if (timeout)
			timer = setTimeout(function() {
				self.abort()
				xhr.ontimeout()
			}, timeout * 1000)
		self.onreadystatechange = function () {
			if (self.readyState == 4) {
				if (timeout) clearTimeout(timer)
				xhr.status = self.status
				if (xhr.status == 200 ||
				    xhr.status == 201 ||
				    xhr.status == 204)
					xhr.onready()
				else xhr.onfail()
			}
		}
		self.open(via, uri + 
				(via == 'GET' && params ? "?" + params : ""), true)
		if (via == 'POST') {
			self.setRequestHeader("Content-type", 
					"application/x-www-form-urlencoded")
		}
		self.send(via == 'GET' && !params ? null : params)
	}

	this.getText = function() { return self.responseText }
	this.getXML = function() { return self.responseXML }
	this.getHeader = function(name) { return self.getResponseHeader(name) }
}

/* misc tools */

function param_format(p) {
	return p ? (p instanceof Object && !(p instanceof String) ? 
		param_encode(p) : p) : ''
}

function param_encode(o) {
	var ls = []
	for (var i in o)
		if (o.hasOwnProperty(i)) 
			ls.push(encodeURIComponent(i) + '=' + 
					encodeURIComponent(o[i]))
	return ls.join('&')
}

function as_array(col) {
	// fix for IE
	if (col instanceof NodeList || col instanceof HTMLCollection) {
		var arr = []
		for (var i = 0; i < col.length; i++)
			arr.push(col[i])
		return arr
	}
	return Array.prototype.slice.call(col)
}

function set_cookie(name, value, expires, path, domain, secure) {
	// set time, it's in milliseconds
	var today = new Date()
	today.setTime(today.getTime())

	if (expires)
		expires = expires * 1000 * 60 * 60 * 24
	var expires_date = new Date(today.getTime() + expires)

	document.cookie = name + "=" + encodeURIComponent(value) +
		(expires ? ";expires=" + expires_date.toGMTString() : "" ) +
		(path ? ";path=" + path : "" ) +
		(domain ? ";domain=" + domain : "" ) +
		(secure ? ";secure" : "" )
}

function get_cookie(name) {
	var results = document.cookie.match
		('(^|;) ?' + name + '=([^;]*)(;|$)')
	if (results)
		return decodeURIComponent(results[2])
	else return ""
}

function add_row(table, row) {
	var tbody
	if (!(tbody = $$(table, 'tbody')[0])) tbody = table
	var tr = $make('tr')
	$add(tbody, tr)
	for (var i = 0; i < row.length; i++)
		$add(tr, $make('td', '', '', row[i]))
	return table
}

function del_row(child) {
	for (var e = child; e != null; e = e.parentNode) {
		if (e.nodeName.toLowerCase() == 'tr') {
			$del(e)
			return
		}
	}
}

function make_form(action, method) {
	return $make('form', {'action': action, 'method': method})
}

function get_checked(e) {
	var ls = $$(e, 'input')
	for (var i = 0; i < ls.length; i++)
		if (ls[i].checked)
			return ls[i].value
	return null
}

function set_text(e, str) {
	document.all ? e.innerText = str : e.textContent = str
}

function get_text(e, str) {
	return document.all ? e.innerText : e.textContent
}

function append_text(e, str) {
	document.all ? e.innerText += str : e.textContent += str
}

function get_childlist(e) {
	var l = []
	for (var i = 0; i < e.childNodes.length; i++)
		if (e.childNodes[i].nodeType == 1)
			l.push(e.childNodes[i])
	return l
}

function repeat(o, n) {
	if (!n)
		return ""
	else
		return o + repeat(o, n-1)
}

/* convenient DOM tools */

var $ = function() {
	var _mem = {}
	return function(node, id) {
		if (!id) {
			id = node
			node = document
		}
		if (id in _mem && _mem[id] && _mem[id].parentNode)
			return _mem[id]
		return _mem[id] = node.getElementById(id)
	}
}()

var $$ = function(node, tag) {
	if (!tag) {
		tag = node
		node = document
	}
	return node.getElementsByTagName(tag)
}

function $node(o) {
	return typeof o == 'string' || o instanceof String ? 
			document.createTextNode(o) : o
}

function $make(tagName, id, className, innerHTML) {
	var e = document.createElement(tagName)
	if (id && id instanceof Object) {
		for (var i in id)
			e.setAttribute(i, id[i])
		if (className) e.innerHTML = className
	} else {
		if (id) e.id = id
		if (className) e.className = className
		if (innerHTML) e.innerHTML = innerHTML
	}
	return e
}

function $add(node, ls) {
	if (ls instanceof Array)
		for (var i = 0; i < ls.length; i++)
			node.appendChild($node(ls[i]))
	else for (var i = 1; i < arguments.length; i++)
		node.appendChild($node(arguments[i]))
	return node
}

function $insert(node, ls) {
	var first = node.firstChild
	if (ls instanceof Array)
		for (var i = 0; i < ls.length; i++)
			node.insertBefore($node(ls[i]), first)
	else for (var i = 1; i < arguments.length; i++)
		node.insertBefore($node(arguments[i]), first)
	return node
}

function $replace(node, o) {
	node.parentNode.replaceChild($node(o), node)
	return node
}

function $del(ls) {
	if (arguments.length > 1) ls = as_array(arguments)
	if (ls instanceof Array)
		for (var i = 0; i < ls.length; i++)
			ls[i].parentNode.removeChild(ls[i])
	else ls.parentNode.removeChild(ls)
}
