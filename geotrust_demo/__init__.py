import flask
import os
import mz
import re
import thread
import atexit

app = flask.Flask(__name__)
jqueue = mz.Queue()
jpool = mz.Pool()


def cleanup():
    jqueue.join()
    jpool.close()


@app.before_first_request
def startup():
    atexit.register(cleanup)

    for i in range(os.sysconf('SC_NPROCESSORS_ONLN')):
        thread.start_new_thread(run_job, ())
    jpool.start(1 if mz.KEEP_TIME > 2.0 else mz.KEEP_TIME / 2.0)


def run_job():
    while 1:
        try:
            # super safe...
            with jqueue.get() as mj:
                # ensure the existence of log and proc
                jpool.checkin(mj)
        except Exception, e:
            app.logger.error('%s: %s' % (e.__class__.__name__, str(e)))
        finally:
            # log the error, don't hang
            jqueue.task_done()


@app.route("/")
def root():
    return app.send_static_file('frames_example_6.html')


@app.route("/<path:path>")
def web(path):
    return app.send_static_file(path)


@app.route("/demo/<unit>")
def demo(unit):
    return flask.make_response(flask.render_template(unit + '.html'))


@app.route("/demo/<unit>/<subcmd>", methods=['POST'])
def execute_demo(unit, subcmd):
    if not all(map(re.compile(r'^[\w-]+$').match, [unit, subcmd])):
        flask.abort(403)
    script = os.path.join(os.path.dirname(__file__),
                          'libexec',
                          'demo_{0}_{1}.sh'.format(unit, subcmd))
    if not os.path.isfile(script):
        flask.abort(403)
    mj = mz.Job(cmd=['sh', script])
    jqueue.put(mj)
    return '', 201, {'Location': flask.url_for('job_monitor', jid=mj.id)}


@app.route("/demo/job/<jid>")
def job_monitor(jid):
    try:
        mj = jpool[jid]
        if not hasattr(mj, 'monitor'):
            mj.monitor = open(mj.pathof('output.log'))
        # reset fp
        mj.monitor.seek(0, os.SEEK_CUR)
        data = mj.monitor.read()
        if mj.proc.returncode is not None:
            mj.monitor.close()
        return data, 200, {'Content-Type': 'text/plain'}
    except:
        flask.abort(404)


if __name__ == "__main__":
    app.run()
